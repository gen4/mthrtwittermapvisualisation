function MMap(readyCallback){
  var map;
  var that = this;
  this.center = [55.7539, 37.6207];
  function initialize() {
    var MY_MAPTYPE_ID = 'custom_style';

    var fbstyle=[
      {
        elementType: 'labels',
        stylers: [
          { visibility: 'off' }
        ]
      },
      {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
          {
            "color": "#3b5998"
          }
        ]
      },
      {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "all",
        "stylers": [
          {
            "hue": "#3b5998"
          },
          {
            "saturation": -22
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#f7f7f7"
          },
          {
            "saturation": 10
          },
          {
            "lightness": 76
          }
        ]
      },
      {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
          {
            "color": "#f7f7f7"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
          {
            "color": "#8b9dc3"
          }
        ]
      },
      {
        "featureType": "administrative.country",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "visibility": "simplified"
          },
          {
            "color": "#3b5998"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#8b9dc3"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "simplified"
          },
          {
            "color": "#8b9dc3"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "all",
        "stylers": [
          {
            "invert_lightness": false
          },
          {
            "color": "#ffffff"
          },
          {
            "weight": 0.43
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#8b9dc3"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#3b5998"
          }
        ]
      }
    ]


    var featureOpts = [
      {
        stylers: [
          { hue: '#890000' },
          { visibility: 'simplified' },
          { gamma: 0.5 },
          { weight: 0.5 }
        ]
      },
      {
        elementType: 'labels',
        stylers: [
          { visibility: 'off' }
        ]
      },
      {
        featureType: 'water',
        stylers: [
          { color: '#890000' }
        ]
      }
    ];



    var mapOptions = {
      zoom: 10,
      disableDefaultUI: true,
      center: new google.maps.LatLng(that.center[0],that.center[1]),
      mapTypeId: MY_MAPTYPE_ID
    };

    var pc_event = new Event('projectionChanged');

    map = new google.maps.Map(document.getElementById('mainMap'),
      mapOptions);
      var overlay = new google.maps.OverlayView();

    overlay.onAdd = function() {
      that.layer = d3.select(this.getPanes().overlayLayer).append("svg");
      that.layer.style('overflow','visible')
      that.layer.style('ponter-events','none');
      that.layers = {};
      that.layers['center'] = that.layer.append('g').style('opacity',0.7);//.attr('filter',"url(#f1)");
      that.layers['country'] = that.layer.append('g').style('opacity',0.7);
      that.prj = this.getProjection();
      readyCallback(null,that);
    };



    overlay.draw = function() {

      document.dispatchEvent(new CustomEvent('projectionChanged', { 'detail': 'oue' }));
      that.prj = this.getProjection();

    }
    overlay.setMap(map);
    var styledMapOptions = {
      name: 'Custom Style'
    };

    var customMapType = new google.maps.StyledMapType(fbstyle, styledMapOptions);
    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

  }

  google.maps.event.addDomListener(window, 'load', initialize);
}



MMap.prototype.setData = function(data){
  this.setRemoveMarkers('center',data.center);
  this.setRemoveMarkers('country',data.country);

};

var I = function(d){return d.link;};

var endingDuration = 1445;
//d3.selection.prototype.checkinterrupt = function() {
//  return this.each(function(d) {
    //var lock = this.__transition__;
    //console.log('.', d3.select(this));
    //endingDuration
    //if (lock) ++lock.active;
    //if (lock && d.active) {
    //  lock.active = 0;
    //}
  //});
//};

MMap.prototype.setRemoveMarkers = function(type,data){

  var that = this;
  var marker = this.layers[type].selectAll("circle").data(data,I);



  marker.enter()
    .append('circle')
    .style("pointer-events", "none")
    .attr("fill",  function(d){return d.center==1?"#EE8783":'#6bcba1'})
    .attr('cx', function(d){return d.x1})
    .attr('cy', function(d){return d.y1})
    .attr('id', function(d){return d.id})
    .attr('r', 0)
    .transition()
      .ease('back-out')
      .delay(Math.random()*300)
      .duration(400)
      .attr('r', 8);

  marker
    .attr('cx', function(d){return d.x1})
    .attr('cy', function(d){return d.y1})
    .each(function(d){
      var item = d3.select(this)
      if(item.attr('removing')){
        item.transition()
        item.attr('removing',false)
        item.transition()
          .ease('back-out')
          .delay(Math.random()*300)
          .duration(400)
          .attr('r', 8);
      }
    });


  marker
    .exit()
    .attr('removing',true)
    .transition()
              .delay(Math.random()*300)
              .ease('exp-out')
              .duration(endingDuration)
              .attr('r', 0)
              .remove();
};


MMap.prototype.calculateCornersCoordinates = function(){
  this.corners = {}
  this.corners.tl=this.pixelToGeo(0,0);
  this.corners.br=this.pixelToGeo(720,400);
}

MMap.prototype.pixelToGeo = function(x,y){
  return this.projection.fromGlobalPixels(mainMap.converter.pageToGlobal([x,y]),mainMap.getZoom());
};

MMap.prototype.geoToPixel = function(lat, long){
  //return function(lnglat) {
    var ret = this.prj.fromLatLngToDivPixel(new google.maps.LatLng(lat,long));

    return [ret.x, ret.y]
  //};
  //return mainMap.converter.globalToPage(this.projection.toGlobalPixels([lat,long], mainMap.getZoom()));
};

