function UIWindow(div,readyCallback){
  this.div = $('#'+div);
  this.wrapper  = $('#ui-window-wrapper');
  var that = this;
  var header = this.header = $('.ui-header',this.div);
  var content = this.content = $('.ui-content',this.div);

  document.addEventListener('projectionChanged', function (e) {
    console.log(' > projectionChanged ');
    that.recalculateAllData();
    that.refreshData();
  }, false);

  this.side_paddings=15;

  this.bounds = {
    min:30,
    max:60
  };

  // Header show/hide
  this.hidingBehaviorOn();



  // Header behavior
  this.header.on('mousedown',function(e_down){
    that.hidingBehaviorOff();
    that.div.removeClass('ui-header-open-animation');


     $(document).on('mousemove',function(e){
       that.wrapper.css('left',(e.pageX-e_down.offsetX)+'px');
       that.wrapper.css('top', (e.pageY-e_down.offsetY+34)+'px');
    });

    $(document).on('mouseup',function(e){
      that.hidingBehaviorOn();
      $(document).unbind(e);
      $(document).unbind('mousemove');
      that.div.addClass('ui-header-open-animation');

    });
  });
  readyCallback(null,this)
}


UIWindow.prototype.hidingBehaviorOn = function(){
  var that = this;
  this.div.on('mouseover',function(){
    that.header.addClass('opened');
    that.div.addClass('opened');
    $('#slider_min').addClass('opened');
    $('#slider_max').addClass('opened');
    $('.ui-digits').addClass('opened')

  });

  this.div.on('mouseout',function(){
    that.header.removeClass('opened')
    that.div.removeClass('opened');
    $('#slider_min').removeClass('opened');
    $('#slider_max').removeClass('opened');
    $('.ui-digits').removeClass('opened');
  });
};


UIWindow.prototype.hidingBehaviorOff = function(){
  this.div.unbind('mouseover');
  this.div.unbind('mouseout');
};


UIWindow.prototype.setVLayer = function(vlayer){

  this.vlayer = vlayer;
  this.initHisto();
  this.initInterface();


};

UIWindow.prototype.moveSlider = function(event,slider_type,xoffset) {
  var xpos= event[0];

  var that = this;
  var m = xpos;

  if (slider_type == 'min') {
    m = bounds(m,this.side_paddings,this.smax.style('left').split('px')[0]);
    this.smin.style('left', m + 'px');
  } else if (slider_type == 'max') {
    m = bounds(m,this.smin.style('left').split('px')[0],this.histo_size.width - this.side_paddings);
    this.smax.style('left', m + 'px');
  } else {
      var m = m-xoffset;//normalized_xpos
      var diff =  this.smax.style('left').split('px')[0]- this.smin.style('left').split('px')[0];
      m = bounds(m,this.side_paddings,this.histo_size.width - this.side_paddings - diff);
      this.smin.style('left', m + 'px');
      this.smax.style('left', (m+diff) + 'px');
  }
  var diff =  this.smax.style('left').split('px')[0]- this.smin.style('left').split('px')[0];

  this.midslider.style('left',  this.smin.style('left'));
  this.midslider.style('width',  this.smax.style('left').split('px')[0]- this.smin.style('left').split('px')[0] + 'px');



  var minDayOffset = Math.round((this.smin.style('left').split('px')[0] - this.side_paddings)/that.work_area.width * that.durationInDays);
  var maxDayOffset = Math.round((this.smax.style('left').split('px')[0] - this.side_paddings)/that.work_area.width * that.durationInDays);

  if (slider_type == 'min') {
    that.bounds.min = moment(that.minDate).add(minDayOffset, 'days');
  } else if (slider_type == 'max') {
    that.bounds.max = moment(that.minDate).add(maxDayOffset, 'days');
  } else{
    that.bounds.min = moment(that.minDate).add(minDayOffset, 'days');
    that.bounds.max = moment(that.minDate).add(maxDayOffset, 'days');
  }

  this.refreshData();
  this.refreshHisto();

  var fontsize = diff*0.3;
  fontsize = bounds(fontsize,20,75);
  m = bounds(m,this.side_paddings,this.smin.style('left').split('px')[0]);

  d3.select('#uiTotalPoints')
    .style('font-size',fontsize+'px')
    .style('line-height',fontsize+'px')

    .style('width',diff+'px')
    .style('left',m + 'px');

  d3.select('#uiCenterPoints')
    .style('width',diff+'px')
    .style('left', m + 'px');

  d3.select('#uiCountryPoints')
    .style('width',diff+'px')
    .style('left', m + 'px');

  d3.select('#uiMinDateText')
    .style('left', (m-$('#uiMinDateText').width()) + 'px');

  d3.select('#uiMaxDateText')
    .style('left', this.smax.style('left'));
};

UIWindow.prototype.refreshData = function(){
  var that = this;
  var totalPoints = 0;
  var pointsByGroup = {center:0,country:0};

  _(that.flatDataSet).each(function (group,key) {
    _.each(group,function(item){
      item.active = item.date.isBetween(that.bounds.min, that.bounds.max);
      totalPoints+=item.active?1:0;
      pointsByGroup[key]+=item.active?1:0;
    })
  });

  d3.select('#uiTotalPoints')
    .text(totalPoints)

  d3.select('#uiCenterPoints')
    .text(pointsByGroup.center)

  d3.select('#uiCountryPoints')
    .text(pointsByGroup.country);

  d3.select('#uiMinDateText')
    .text(this.bounds.min.format('ll'));

  d3.select('#uiMaxDateText')
    .text(this.bounds.max.format('ll'));

  this.mmap.setData({
                      center:_.filter(this.flatDataSet.center,{active:true}),
                      country:_.filter(this.flatDataSet.country,{active:true})
  });
};


UIWindow.prototype.initInterface = function(){
  this.smin = d3.select('#slider_min')
    .style('left',this.side_paddings+'px');


  var that = this;

  d3.select('#ui-window-wrapper')
    .style('right','100px')
    .style('top','60px')
    .transition()
    .duration(500)
    .style('opacity',1);



  var bg = d3.select('#histo1');
  var doc = d3.select(document);

    this.smin.on('mousedown',function(){
      that.smin.classed('animated',false);
      $('.ui-digits').removeClass('ui-header-open-animation');

      bg.on('mousemove',function(){that.moveSlider(d3.mouse(this),'min')})
      doc.on('mouseup',function(){
        $('.ui-digits').addClass('ui-header-open-animation');
        that.smin.classed('animated',true);
        bg.on('mousemove',null)
        doc.on('mouseup',null)
      })
    });

 this.smax = d3.select('#slider_max')
    .style('left',(this.histo_size.width - this.side_paddings)+'px')
     .on('mousedown',function(){
     $('.ui-digits').removeClass('ui-header-open-animation');
     that.smax.classed('animated',false);
       bg.on('mousemove',function(){that.moveSlider(d3.mouse(this),'max')})
       doc.on('mouseup',function(){
         $('.ui-digits').addClass('ui-header-open-animation');
         that.smax.classed('animated',true);
         bg.on('mousemove',null)
         doc.on('mouseup',null)
       })
     })

  var xoffset=0;
  this.midslider = d3.select('#slider_mid')
    .on('mousedown',function(){
      $('.ui-digits').removeClass('ui-header-open-animation');

      that.smin.classed('animated',false);
      that.smax.classed('animated',false);
      xoffset = d3.mouse(this)[0];
      bg.on('mousemove',function(){that.moveSlider(d3.mouse(this),'mid',xoffset)})
      doc.on('mouseup',function(){
        $('.ui-digits').addClass('ui-header-open-animation');
        that.smin.classed('animated',true);
        that.smax.classed('animated',true);
        bg.on('mousemove',null)
        doc.on('mouseup',null)
      })
    })

  var diff = this.histo_size.width - this.side_paddings;
  var m = this.side_paddings;
  var fontsize = diff*0.5
  fontsize = bounds(fontsize,20,75);
  d3.select('#uiTotalPoints')
    .style('font-size',fontsize+'px')
    .style('line-height',fontsize+'px')
    .style('width',diff+'px')
    .style('left', m + 'px');

  d3.select('#uiCenterPoints')
    .style('width',diff+'px')
    .style('left', m + 'px');

  d3.select('#uiCountryPoints')
    .style('width',diff+'px')
    .style('left', m + 'px');

  d3.select('#uiMinDateText')
    .style('left', (m-$('#uiMinDateText').width()) + 'px');

  d3.select('#uiMaxDateText')
    .style('left', (m+diff) + 'px');
};

UIWindow.prototype.recalculateAllData = function(){
  var that = this;
  this.flatDataSet = _.each(this.flatDataSet,function(group){
    _.each(group,that.calcPointsVisData,that);
  });
};

UIWindow.prototype.calcPointsVisData = function(d){
  var centerInPixels = this.mmap.prj.fromLatLngToDivPixel(new google.maps.LatLng(this.mmap.center[0],this.mmap.center[1]));
  var lat = parseFloat(d.lat.replace(',','.'));
  var long = parseFloat(d.long.replace(',','.'));
  var ll = new google.maps.LatLng(lat,long);
  var pos =this.mmap.prj.fromLatLngToDivPixel(ll);
  d.pos = pos;
  d.dist = Math.sqrt(Math.pow(centerInPixels.x-pos.x,2)+Math.pow(centerInPixels.y-pos.y,2))
  d.path = d.dist*.5;//Math.random()*100+100
  d.dir = Math.atan2(centerInPixels.y- pos.y,centerInPixels.x-pos.x);
  if(d.center==0){
    d.x1=pos.x;
    d.y1=pos.y;
    d.x2=pos.x-(d.path*Math.cos(d.dir));
    d.y2=pos.y-(d.path*Math.sin(d.dir));
  }else{
    d.x2=pos.x;
    d.y2=pos.y;
    d.x1=pos.x-(d.path*Math.cos(d.dir));
    d.y1=pos.y-(d.path*Math.sin(d.dir));
  }

}


UIWindow.prototype.initHisto = function(){

  var that = this;
  this.minDate = moment('01.01.3000', "DD.MM.YYYY");
  this.maxDate = moment('01.01.1000', "DD.MM.YYYY");

  this.totalDays = 0;
  this.totalCenter = 0;
  this.totalCountry = 0;
  this.maxTwittsPerDay = 0;
  this.totalTwitts = this.vlayer.dataSet.length ;


  this.dataSetByDate = _.chain(this.vlayer.dataSet)
                          .groupBy('date')
                          .map(function(item,key){
                            var groupedByCenter = _.groupBy(item,'center');

                            var obj = {
                              center:groupedByCenter[1] || [],
                              country:groupedByCenter[0] || [],
                              active:true,
                              date:moment(key, "DD.MM.YYYY")
                            };

                            that.totalCenter+=obj.center.length || 0;
                            that.totalCountry+=obj.country.length || 0;
                            that.maxTwittsPerDay = Math.max(that.maxTwittsPerDay,obj.center.length,obj.country.length)

                            return obj;
                          })
                          .sortBy(function(item){

                            that.minDate = moment.min(that.minDate,item.date);
                            that.maxDate = moment.max(that.maxDate,item.date);

                            return item.date.unix();
                          })
                         .value();

  this.flatDataSet = {center:_.flatten(that.dataSetByDate,'center'),country:_.flatten(that.dataSetByDate,'country')};
   _.each(this.flatDataSet,function(group){
     _.each(group,function(item) {
       item.date = moment(item.date, "DD.MM.YYYY");
     });
   });
  this.bounds.min = that.minDate;
  this.bounds.max = that.maxDate;
  this.totalDays = this.dataSetByDate.length;
  this.items_padding=2;
  this.histo_size = {
    width:300,
    height:150
  };

  this.work_area = {
    width:this.histo_size.width-this.side_paddings*2,
    height:this.histo_size.height-this.side_paddings*2
  }

  this.bounds.min = this.minDate;
  this.bounds.max = this.maxDate;
  this.durationInDays = this.maxDate.diff(this.minDate, 'days');
  //var tmpMoment = moment(that.minDate)
  //var minDayOffset = Math.round(t * that.durationInDays);
  //if(this.oldOffset == minDayOffset){return;}
  this.oldOffset = this.minDate;
  console.log('dataSetByDate', this.dataSetByDate);
  console.log('minDate:',this.minDate.format('LL'));
  console.log('maxDate:',this.maxDate.format('LL'));
  console.log('totalDays:',this.totalDays," \t totalTwitts:",this.totalTwitts);
  console.log('totalCenter:',this.totalCenter," \t totalCountry:",this.totalCountry);


  this.buildHisto()

};

UIWindow.prototype.buildHisto = function(){
  var that = this;
  this.rectStep = this.work_area.width/this.totalDays;
  this.rectWidth = (this.work_area.width - this.items_padding*(this.totalDays-1))/this.totalDays;


  var svg = d3.select('#histo');

  var group = svg.selectAll('g')
    .data(this.dataSetByDate);

  group.enter().append("g")
  group.exit().remove();

  //group


  var rects = group.selectAll('rect')
    .data(function(d,i) { return [{data:d, number:i}] })

  rects.enter().append("rect")
  rects.exit().remove();


  //Center rects
  rects.attr('width',this.rectWidth)
    .attr('height',function(d){return 0;})
    .attr("x", function(d){return that.rectStep* d.number+that.side_paddings+that.items_padding;})
    .attr("y", function(){return that.work_area.height/2+that.side_paddings;})
    .classed('center',true)
    .classed('unactive',function(d){ return !d.data.active})
    .transition()
    .delay(function(d){return d.number*10+100})
    .duration(1000)
    .ease("elastic")
    .attr('height',function(d){ return d.data.center.length/that.maxTwittsPerDay*that.work_area.height/2;})

  //country rects
  rects.enter().append("rect")
    .attr('width',that.rectWidth)
    .attr('height',0)
    .attr("x", function(d){ return that.rectStep* d.number+that.side_paddings+that.items_padding;})
    .attr("y", function(d){ return that.work_area.height/2+that.side_paddings;})
    .classed('country',true)
    .classed('unactive',function(d){ return !d.data.active})
    .transition()
    .delay(function(d){return d.number*10+100})
    .duration(1000)
    .ease("elastic")
    .attr('height',function(d){return d.data.country.length/that.maxTwittsPerDay*that.work_area.height/2})
    .attr("y", function(d){return that.work_area.height/2+that.side_paddings- (d.data.country.length/that.maxTwittsPerDay*that.work_area.height/2);})

};

UIWindow.prototype.refreshHisto = function(){
  var that = this;

  var rects = d3.select('#histo').selectAll('g').selectAll('rect')
    //.data(function(d){return d;})
    .classed('unactive',function(d){return !d.data.date.isBetween(that.bounds.min, that.bounds.max);});


  //.attr('height',function(d){console.log(',',d); return 44;})
  //console.log(rects)
  //var group = svg;
  //var rects = group;
  //rects.data(function(d){ return d;})
  //  .enter()
  //  .attr('height',function(d){console.log(',',d); return 44;})
  //  .classed('unactive',function(d){ console.log(d);return d.unactive})
    //.transition()
    //.duration(1000)


  //  rects.data(function(d,i) { return [{data:d, number:i}] })
}

UIWindow.prototype.setMMap=function(mmap){
  this.mmap = mmap;
  console.log('..mmmap',mmap);
}


function bounds(val,min,max){
  if(val<min){
    return min;
  }else if(val>max){
    return max;
  }else{
    return val;
  }
}
