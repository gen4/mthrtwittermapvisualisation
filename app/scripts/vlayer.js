/**
 * Created by gen4 on 17/01/15.
 */
"use strict"

function VLayer(readyCallback){
  var that = this;
  this.getData(function(error){
    readyCallback(error,that);
  });

}

VLayer.prototype.getData = function(done){
  $.blockUI({ css: {
    border: 'none',
    padding: '15px',
    backgroundColor: '#000',
    '-webkit-border-radius': '10px',
    '-moz-border-radius': '10px',
    opacity: .5,
    color: '#fff'
  } });
  var that = this;
  d3.dsv(";", "text/plain")("../data/data_utf.csv", function(error, data) {
    if(!error){
      $.unblockUI()
      console.log('Data recived',data);
      that.dataSet = data;

    }else{
      console.error(error);
    }
    done(error);
  })
};

VLayer.prototype.setMMap = function(mmap){
  this.mmap = mmap;
}






//VLayer.prototype.showme = function(){
//  this.getData()
//  var layer =d3.select("#vLayer")
//    var circle = layer.selectAll("circle")
//    .data([32, 57, 112,2,88],function(d){return d;});
//
//circle.enter().append("circle")
//    .attr("cy", 60)
//    .attr("cx", function(d, i) { return i * 100 + 30; })
//    .attr("r", function(d) { return Math.sqrt(d); });
//
//circle.exit().remove()
//};

